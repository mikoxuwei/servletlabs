<%@page import="com.mycompany.servletlabs.Account"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Start Page</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <a href="ClearServlet">Clear</a><br/>
        <form method="post" action="AddAccountServlet">
            
            <%
                Account account=(Account)session.getAttribute("account");
                if(account==null){
                    account=new Account();
                }
            %>
            
            Id: <input name="id" value="<%=account.getId()%>"/><br/>
            Password: <input name="password" type="password" value="<%=account.getPassword()%>"/><br/>
            Email: <input name="email" value="<%=account.getEmail()%>"/><br/>
            Mobile: <input name="mobile" value="<%=account.getMobile()%>"/><br/>
            <input type="submit"/>
        </form>
    </body>
</html>
